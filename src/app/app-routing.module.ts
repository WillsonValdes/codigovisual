import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenComponent } from './pages/examen/examen.component';


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';

const routes: Routes = [
{ path:"examen",component:ExamenComponent,children:[
  {
    path:'edicion/:id',component:ExamenEdicionComponent
  },
  {
    path:'nuevo',component:ExamenEdicionComponent
  }
]
},
{ path:"especialidad",component:EspecialidadComponent, children:[
  {
    path:'nuevo',component:EspecialidadEdicionComponent
  }
]
}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
