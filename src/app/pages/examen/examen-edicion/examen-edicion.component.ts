import { ExamenService } from './../../../services/examen.service';
import { Examen } from './../../../model/examen';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-examen-edicion',
  templateUrl: './examen-edicion.component.html',
  styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  examen:Examen;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private examenService:ExamenService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'descripcion':new FormControl(''),            
    }); 
  }
  initForm(){
    if(this.edicion){
      this.examenService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'descripcion':new FormControl(data.descripcion),
        });
      })
        
    }
  }
  ngOnInit() {
    this.examen=new Examen();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.examen.id=this.form.value['id'];
    this.examen.descripcion=this.form.value['descripcion'];        
    if(this.edicion){

      this.examenService.modificar(this.examen).subscribe(data=>
      {
        this.examenService.listar().subscribe(examenes => {this.examenService.examenCambio.next(examenes);       
        this.examenService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.examenService.registar(this.examen).subscribe(data=>{
        this.examenService.listar().subscribe(examenes => {this.examenService.examenCambio.next(examenes);       
        this.examenService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['examen']);
  }

}
